//
//  FlickrSearchResults.swift
//  FlickrSearch
//
//  Created by ANDROMEDA on 9/5/16.
//  Copyright © 2016 infancyit. All rights reserved.
//

import Foundation

struct FlickrSearchResults {
  let searchTerm : String
  let searchResults : [FlickrPhoto]
}
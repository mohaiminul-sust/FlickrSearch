//
//  FlickrPhoto.swift
//  FlickrSearch
//
//  Created by ANDROMEDA on 9/5/16.
//  Copyright © 2016 infancyit. All rights reserved.
//

import UIKit

struct FlickrPhoto : Equatable {
  var thumbnail : UIImage?
  var largeImage : UIImage?
  let photoID : String
  let farm : Int
  let server : String
  let secret : String
  
  init (photoID:String,farm:Int, server:String, secret:String) {
    self.photoID = photoID
    self.farm = farm
    self.server = server
    self.secret = secret
  }
  
  func flickrImageURL(size:String = "m") -> NSURL? {
    if let url =  NSURL(string: "https://farm\(farm).staticflickr.com/\(server)/\(photoID)_\(secret)_\(size).jpg") {
      return url
    }
    return nil
  }
  
  mutating func loadLargeImage(completion: (flickrPhoto:FlickrPhoto, error: NSError?) -> Void) {
    guard let loadURL = flickrImageURL("b") else {
      NSOperationQueue.mainQueue().addOperationWithBlock({
        completion(flickrPhoto: self, error: nil)
      })
      return
    }
    
    let loadRequest = NSURLRequest(URL:loadURL)
    
    NSURLSession.sharedSession().dataTaskWithRequest(loadRequest) { (data, response, error) in
      if let error = error {
        NSOperationQueue.mainQueue().addOperationWithBlock({
          completion(flickrPhoto: self, error: error)
        })
        return
      }
      
      guard let data = data else {
        NSOperationQueue.mainQueue().addOperationWithBlock({
          completion(flickrPhoto: self, error:nil)
        })
        return
      }
      
      let returnedImage = UIImage(data: data)
      self.largeImage = returnedImage
      NSOperationQueue.mainQueue().addOperationWithBlock({
        completion(flickrPhoto: self, error: nil)
      })
      
      return
      
      }.resume()
  }
  
  func sizeToFillWidthOfSize(size:CGSize) -> CGSize {
    
    guard let thumbnail = thumbnail else {
      return size
    }
    
    let imageSize = thumbnail.size
    var returnSize = size
    
    let aspectRatio = imageSize.width / imageSize.height
    
    returnSize.height = returnSize.width / aspectRatio
    
    if returnSize.height > size.height {
      returnSize.height = size.height
      returnSize.width = size.height * aspectRatio
    }
    
    return returnSize
  }
  
}

func == (lhs: FlickrPhoto, rhs: FlickrPhoto) -> Bool {
  return lhs.photoID == rhs.photoID
}
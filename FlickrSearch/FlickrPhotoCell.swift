//
//  FlickrPhotoCell.swift
//  FlickrSearch
//
//  Created by ANDROMEDA on 9/5/16.
//  Copyright © 2016 infancyit. All rights reserved.
//

import UIKit

class FlickrPhotoCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
}

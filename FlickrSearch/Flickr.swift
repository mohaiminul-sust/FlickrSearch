//
//  Flickr.swift
//  FlickrSearch
//
//  Created by ANDROMEDA on 9/5/16.
//  Copyright © 2016 infancyit. All rights reserved.
//
	
import UIKit

let apiKey = "61d31c9ce10e60805a08a775576f46e0"

class Flickr {
  
  let processingQueue = NSOperationQueue()
  
  func searchFlickrForTerm(searchTerm: String, completion : (results: FlickrSearchResults?, error : NSError?) -> Void){
    
    guard let searchURL = flickrSearchURLForSearchTerm(searchTerm) else {
      let APIError = NSError(domain: "FlickrSearch", code: 0, userInfo: [NSLocalizedFailureReasonErrorKey:"Unknown API response"])
      completion(results: nil, error: APIError)
      return
    }
    
    let searchRequest = NSURLRequest(URL: searchURL)
    
    NSURLSession.sharedSession().dataTaskWithRequest(searchRequest) { (data, response, error) in
      
      if let _ = error {
        let APIError = NSError(domain: "FlickrSearch", code: 0, userInfo: [NSLocalizedFailureReasonErrorKey:"Unknown API response"])
        NSOperationQueue.mainQueue().addOperationWithBlock({
          completion(results: nil, error: APIError)
        })
        return
      }
      
      guard let _ = response as? NSHTTPURLResponse,
        data = data else {
          let APIError = NSError(domain: "FlickrSearch", code: 0, userInfo: [NSLocalizedFailureReasonErrorKey:"Unknown API response"])
          NSOperationQueue.mainQueue().addOperationWithBlock({
            completion(results: nil, error: APIError)
          })
          return
      }
      
      do {
        
        guard let resultsDictionary = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions(rawValue: 0)) as? [String: AnyObject],
        stat = resultsDictionary["stat"] as? String else {
          
          let APIError = NSError(domain: "FlickrSearch", code: 0, userInfo: [NSLocalizedFailureReasonErrorKey:"Unknown API response"])
          NSOperationQueue.mainQueue().addOperationWithBlock({
            completion(results: nil, error: APIError)
          })
          return
        }
        
        switch (stat) {
        case "ok":
          print("Results processed OK")
        case "fail":
          if let message = resultsDictionary["message"] {
            
            let APIError = NSError(domain: "FlickrSearch", code: 0, userInfo: [NSLocalizedFailureReasonErrorKey:message])
            
            NSOperationQueue.mainQueue().addOperationWithBlock({
              completion(results: nil, error: APIError)
            })
          }
          
          let APIError = NSError(domain: "FlickrSearch", code: 0, userInfo: nil)
          
          NSOperationQueue.mainQueue().addOperationWithBlock({
            completion(results: nil, error: APIError)
          })
  
          return
        default:
          let APIError = NSError(domain: "FlickrSearch", code: 0, userInfo: [NSLocalizedFailureReasonErrorKey:"Unknown API response"])
          NSOperationQueue.mainQueue().addOperationWithBlock({
            completion(results: nil, error: APIError)
          })
          return
        }
        
        guard let photosContainer = resultsDictionary["photos"] as? [String: AnyObject], photosReceived = photosContainer["photo"] as? [[String: AnyObject]] else {
          
          let APIError = NSError(domain: "FlickrSearch", code: 0, userInfo: [NSLocalizedFailureReasonErrorKey:"Unknown API response"])
          NSOperationQueue.mainQueue().addOperationWithBlock({
            completion(results: nil, error: APIError)
          })
          return
        }
        
        var flickrPhotos = [FlickrPhoto]()
        
        for photoObject in photosReceived {
          guard let photoID = photoObject["id"] as? String,
            farm = photoObject["farm"] as? Int ,
            server = photoObject["server"] as? String ,
            secret = photoObject["secret"] as? String else {
              break
          }
          var flickrPhoto = FlickrPhoto(photoID: photoID, farm: farm, server: server, secret: secret)
          
          guard let url = flickrPhoto.flickrImageURL(),
            imageData = NSData(contentsOfURL: url) else {
              break
          }
          
          if let image = UIImage(data: imageData) {
            flickrPhoto.thumbnail = image
            flickrPhotos.append(flickrPhoto)
          }
        }
              
        NSOperationQueue.mainQueue().addOperationWithBlock({
          completion(results:FlickrSearchResults(searchTerm: searchTerm, searchResults: flickrPhotos), error: nil)
        })
        
      } catch _ {
        completion(results: nil, error: nil)
        return
      }
      
      
      }.resume()
  }
  
  private func flickrSearchURLForSearchTerm(searchTerm:String) -> NSURL? {
    
    guard let escapedTerm = searchTerm.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.alphanumericCharacterSet()) else {
      return nil
    }
    
    let URLString = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(apiKey)&text=\(escapedTerm)&per_page=20&format=json&nojsoncallback=1"
    
    guard let url = NSURL(string:URLString) else {
      return nil
    }
    
    return url
  }
}
